_satellite.pushBlockingScript(function(event, target, $variables){
  var zmagDiv = jQuery('.page-embed.embed-zmag > #myViewerContent');
if(zmagDiv.length == 1) {
  if (zmagDiv.next(0).attr('type') == 'text/javascript') {
    zmagDiv.next(0).remove();
    var zmags_ssl_script = document.createElement('script'); 
    zmags_ssl_script.onload = function () {
      console.log('zmag ssl script loaded');
      var pubid = zmagDiv.data("pubid");
      console.log(pubid);
      var viewer = new com.zmags.api.Viewer();
      viewer.setPublicationID(pubid);
      viewer.setParentElementID('myViewerContent');
      viewer.show();
    };

    zmags_ssl_script.type = 'text/javascript';
    zmags_ssl_script.src = 'https://secure.api.viewer.zmags.com/viewer/viewer.js';
    document.head.appendChild(zmags_ssl_script);
  }
}
});
