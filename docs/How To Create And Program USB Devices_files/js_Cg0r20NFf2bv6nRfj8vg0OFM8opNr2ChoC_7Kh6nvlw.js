/**
 * @file
 * Js for modal registration process.
 */

(function($, win) {
  Drupal.behaviors.penton_modal = {
    attach: function(context, settings) {
      var display = false, wrapper;

      var form = $('form', context);
      if (form.hasClass('penton_modal_submit_on_close')) {
        // This event fires before click from our module, so we need to unbind it.
        $('div a.close-reg-btn.close', context).unbind('click');
      }

      if (window.location.pathname.indexOf('penton_ur_thank_you') !== -1
        && window.location.href.indexOf('notkli=1') === -1
        && typeof _satellite !== 'undefined') {
        // Direct call rules.
        _satellite.track('ADVANCED_REGISTRATION_STEP_3');
      }
      $('button.thank-you-reload', context).click(function () {
        if (typeof _satellite !== 'undefined') {
          // Direct call rules.
          _satellite.track('ADVANCED_REGISTRATION_STEP_4');
        }
        reload_modal_window();
      });
      $('a.close-reg-btn', context).click(function (e) {
        if (form.hasClass('penton_modal_reload_after_close')) {
          reload_modal_window();
        } else if (form.hasClass('penton_modal_submit_on_close')) {
          form.find('input[type=checkbox]').attr('checked', false);
          form.find('button.form-submit').trigger('click');
        }
        e.preventDefault();
      });
      function reload_modal_window() {
        // Delete user cookie set during ctools modal wizard process.
        $.cookie('Drupal.visitor.penton_modal_cache_key', null, { path: '/' });
        window.top.location.reload(true);
      }

      function break_points() {
        wrapper = $('.ctools-modal-wrapper', context);

        var height = wrapper.find('.ctools-modal__inner').outerHeight();

        if($(win).width() <= 400 && $(win).height() <= height) {
          wrapper.addClass('ctools-modal-wrapper__fixed');
          display = 'mobile';
        } else if($(win).height() <= height) {
          wrapper.addClass('ctools-modal-wrapper__fixed');
          display = 'table';
        } else {
          display = false;
          wrapper.removeClass('ctools-modal-wrapper__fixed');
        }
      }
      function reset_left_indent() {
        if(display === 'mobile') {
          $(context).css({
            'left': '0',
            'margin-left': 0
          });
        } else if(display !== 'mobile') {
          $(context).css({
            'left': '50%',
            'margin-left': -($('.ctools-modal-wrapper', context).width() / 2)
          });
        }
      }

      if( typeof $('.ctools-modal-wrapper', context).areaScroll === 'function') {
        var wrapper_scroll = $('.ctools-modal-wrapper', context).areaScroll(reset_left_indent);
        var newsletters_scroll = $('.newsletters-list', context).areaScroll();

        $('.close', context).on('click', function() {
          wrapper_scroll.detachEvents().destroy();
          newsletters_scroll.detachEvents().destroy();
          $(this).off();
        });
      }

      break_points();
      reset_left_indent();

      $(win).resize(function() {
        break_points();
        reset_left_indent();
      });
    }
  };
})(jQuery, window);
;
/**
 * @file
 * Penton user register related js.
 *
 */

(function ($) {
  if ($('#newsletter-signup-load').length) {
    $('#newsletter-signup-load').load(
      '/ajax/penton_newsletter_signup',
      { 'tid': $('#newsletter-signup-load').data('tid') },
      function() {
        Drupal.attachBehaviors(this);
      }
    );
  }
  $.fn.success_newsletter_signup = function (val) {
    $('.newsletter-signup').hide();
    $('a.nl_href_link').click();
  };
  $.fn.nl_signup_open_nl_list_modal = function (val) {
    $('.newsletter-signup').hide();
    var nl_list_url = '/penton_modal/nojs/nl_list';
    var $link = $("<a></a>").attr('href', nl_list_url)
      .addClass('ctools-use-modal-processed ctools-modal-modal-popup-medium')
      .click(Drupal.CTools.Modal.clickAjaxLink);

    Drupal.ajax[nl_list_url] = new Drupal.ajax(nl_list_url, $link.get(0), {
      url: nl_list_url,
      event: 'click',
      progress: { type: 'throbber' }
    });
    $link.click();
  }
}(jQuery));

(function($) {
  Drupal.behaviors.penton_newsletter_signup = {
    attach: function(context, settings) {
      if ($('.newsletter-signup-form', context).length) {
        var country = $('.newsletter-signup-form select.country', context).val();
        $('.newsletter-signup-form select.country', context).on('change', function () {
          penton_newsletter_signup_show_right_terms($(this).val());
          penton_newsletter_signup_show_gdpr_marketing_optin($(this).val());
        });
        penton_newsletter_signup_show_right_terms(country);
        penton_newsletter_signup_show_gdpr_marketing_optin(country);
      }

      function penton_newsletter_signup_show_right_terms(country) {
        var $reg_terms_wrapper = $('.reg-terms-of-service-wrapper .form-item-terms', context);
        $reg_terms_wrapper.toggle(country == 'CA');
        $reg_terms_wrapper.find('input[name="terms"]').attr('checked', country != 'CA');
      }

      function penton_newsletter_signup_show_gdpr_marketing_optin(country) {
        if ($('.marketing-form-fields', context).length) {
          window.click_email = false;
          var comm_channel_email = $('.communication-channel-email-check input', context);
          var gdpr_countries = settings.gdpr_countries;
          if (gdpr_countries[country] === undefined) {
            $('.form-item-marketing-optin', context).show();
            $('.form-item-similar-events-optin', context).show();
            $('.form-item-third-party-optin', context).show();
            $('.form-item-gdpr-marketing-optin', context).hide();
            $('.form-item-gdpr-similar-events-optin', context).hide();
            $('.form-item-gdpr-third-party-optin', context).hide();
            $('.marketing-optin-check input', context).prop('checked', true);
            $('.third-party-optin-check input', context).prop('checked', true);
            comm_channel_email.prop('disabled', true);
            comm_channel_email.prop('checked', true);
            if ($(comm_channel_email).parent().find('input[type=hidden]').length < 1) {
              $(comm_channel_email).parent().append('<input type="hidden" name="communication_channels[email]" value="email">');
            }
          }
          else {
            $('.form-item-marketing-optin', context).hide();
            $('.form-item-similar-events-optin', context).hide();
            $('.form-item-third-party-optin', context).hide();
            $('.form-item-gdpr-marketing-optin', context).show();
            $('.form-item-gdpr-similar-events-optin', context).show();
            $('.form-item-gdpr-third-party-optin', context).show();
            window.click_email = false;
            $('.marketing-optin-check input', context).prop('checked', false);
            $('.third-party-optin-check input', context).prop('checked', false);
            comm_channel_email.prop('disabled', false);
            comm_channel_email.prop('checked', false);
            $(comm_channel_email).parent().find('input[type=hidden]').remove();
          }

          $('.marketing-form-fields input[type=checkbox]', context).change(function(e) {
            e.stopImmediatePropagation();
            if (this.name == 'communication_channels[email]') {
              click_email = $(this).prop('checked');
            }
            var marketing_optin = $('.marketing-optin-check input', context).prop('checked') ? 1 : 0;
            var similar_events_optin = $('.similar-events-optin-check input', context).prop('checked') ? 1 : 0;
            var third_party_optin = $('.third-party-optin-check input', context).prop('checked') ? 1 : 0;
            if ($('.form-item-marketing-optin', context).css('display') != 'none') {
              if (marketing_optin == '0' && similar_events_optin == '0' && third_party_optin == '0' && comm_channel_email.prop('disabled') && comm_channel_email.prop('checked')) {
                comm_channel_email.prop('disabled', false);
                click_email ? comm_channel_email.prop('checked', true) : comm_channel_email.prop('checked', false);
                $(comm_channel_email).parent().find('input[type=hidden]').remove();
              }
              if (marketing_optin == '1' || similar_events_optin == '1' || third_party_optin == '1') {
                comm_channel_email.prop('disabled', true);
                comm_channel_email.prop('checked', true);
                if ($(comm_channel_email).parent().find('input[type=hidden]').length < 1) {
                  $(comm_channel_email).parent().append('<input type="hidden" name="communication_channels[email]" value="email">');
                }
              }
            }
          });

          $('.marketing-form-fields select', context).change(function(e) {
            e.stopImmediatePropagation();
            var gdpr_marketing_optin = $('.form-item-gdpr-marketing-optin select', context).val() == '1' ? 1 : 0;
            var gdpr_similar_events_optin = $('.form-item-gdpr-similar-events-optin select', context).val() == '1' ? 1 : 0;
            var gdpr_third_party_optin = $('.form-item-gdpr-third-party-optin select', context).val() == '1' ? 1 : 0;
            if ($('.form-item-gdpr-marketing-optin', context).css('display') != 'none') {
              if (gdpr_marketing_optin == '0' && gdpr_similar_events_optin == '0' && gdpr_third_party_optin == '0' && comm_channel_email.prop('disabled') && comm_channel_email.prop('checked')) {
                comm_channel_email.prop('disabled', false);
                click_email ? comm_channel_email.prop('checked', true) : comm_channel_email.prop('checked', false);
                $(comm_channel_email).parent().find('input[type=hidden]').remove();
              }
              if (gdpr_marketing_optin == '1' || gdpr_similar_events_optin == '1' || gdpr_third_party_optin == '1') {
                comm_channel_email.prop('disabled', true);
                comm_channel_email.prop('checked', true);
                if ($(comm_channel_email).parent().find('input[type=hidden]').length < 1) {
                  $(comm_channel_email).parent().append('<input type="hidden" name="communication_channels[email]" value="email">');
                }
              }
            }
          });
        }
      }
    }
  };
  Drupal.behaviors.penton_register_from = {
    attach: function(context, settings) {
      if (Drupal.settings.penton_reg_form) {
        var reg_form_url = '/penton_modal/nojs/',
            reg_form_classes = 'ctools-use-modal-processed ';
        if (Drupal.settings.penton_reg_form == 'basic') {
          reg_form_url += 'basic_register',
          reg_form_classes = 'ctools-modal-modal-popup-basic';
        }
        else if (Drupal.settings.penton_reg_form == 'adv') {
          reg_form_url += 'register/advanced',
          reg_form_classes = 'ctools-modal-modal-popup-medium';
        }
        else if (Drupal.settings.penton_reg_form == 'login') {
          reg_form_url += 'login',
          reg_form_classes = 'ctools-modal-modal-popup-login';
        }
        else {
          return;
        }
        Drupal.settings.penton_reg_form = false;

        var $link = $("<a></a>").attr('href', reg_form_url)
          .addClass(reg_form_classes)
          .click(Drupal.CTools.Modal.clickAjaxLink);

        Drupal.ajax[reg_form_url] = new Drupal.ajax(reg_form_url, $link.get(0), {
          url: reg_form_url,
          event: 'click',
          progress: { type: 'throbber' }
        });
        $link.click();
      }
    }
  };
  Drupal.behaviors.penton_omeda_register = {
    attach: function(context, settings) {
      if (typeof Drupal.settings.oly_id !== 'undefined' && typeof Drupal.settings.oly_em !== 'undefined' && typeof Drupal.settings.oly_prod_id !== 'undefined' && (typeof Drupal.settings.oly_id !== 'undefined' && Drupal.settings.oly_id)) {
        var reg_url = "";
        console.log(Drupal.settings.oly_em);
        console.log(Drupal.settings.oly_id);
        console.log(Drupal.settings.oly_prod_id);
        reg_url = '/ajax/omeda_basic_register/' + Drupal.settings.oly_em + '/' + Drupal.settings.oly_id + '/' + Drupal.settings.oly_prod_id;

        if (reg_url !== "") {
          console.log('testinghere omeda');
          $.ajax({
            url: reg_url,
          })
          .done(function( result ) {
            if(result != "") {
              console.log('omeda registration success');
              var reg_form_url = '/penton_modal/nojs/register/advanced';
              var reg_form_classes = 'ctools-modal-modal-popup-medium';
              var $link = $("<a></a>").attr('href', reg_form_url)
                .addClass(reg_form_classes)
                .click(Drupal.CTools.Modal.clickAjaxLink);

              Drupal.ajax[reg_form_url] = new Drupal.ajax(reg_form_url, $link.get(0), {
                url: reg_form_url,
                event: 'click',
                progress: { type: 'throbber' }
              });
              $link.click();
            } else {
              console.log('omeda registration failed');
            }
          });
        }
      }
    }
  };
})(jQuery);
;
/**
 * @file
 * Modifies the file selection and download access expiration interfaces.
 */

var uc_file_list = {};

/**
 * Adds files to delete to the list.
 */
function _uc_file_delete_list_populate() {
  jQuery('.affected-file-name').empty().append(uc_file_list[jQuery('#edit-recurse-directories').attr('checked')]);
}

jQuery(document).ready(
  function() {
    _uc_file_delete_list_populate();
  }
);

// When you (un)check the recursion option on the file deletion form.
Drupal.behaviors.ucFileDeleteList = {
  attach: function(context, settings) {
    jQuery('#edit-recurse-directories:not(.ucFileDeleteList-processed)', context).addClass('ucFileDeleteList-processed').change(
      function() {
        _uc_file_delete_list_populate()
      }
    );
  }
}

/**
 * Give visual feedback to the user about download numbers.
 *
 * TODO: would be to use AJAX to get the new download key and
 * insert it into the link if the user hasn't exceeded download limits.
 * I dunno if that's technically feasible though.
 */
function uc_file_update_download(id, accessed, limit) {
  if (accessed < limit || limit == -1) {

    // Handle the max download number as well.
    var downloads = '';
    downloads += accessed + 1;
    downloads += '/';
    downloads += limit == -1 ? 'Unlimited' : limit;
    jQuery('td#download-' + id).html(downloads);
    jQuery('td#download-' + id).attr("onclick", "");
  }
}
;
