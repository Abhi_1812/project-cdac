_satellite.pushAsyncScript(function(event, target, $variables){
  if(window.location.pathname == "/design-learning-center") {
  jQuery('.article-teaser__content > .article-teaser__header > .sp-title').remove();
  jQuery('article.article-teaser.article-teaser__sponsored').attr('style', 'border-top-color: #6e0000 !important');
  jQuery('.ccc-usermarketing-promo.sponsored').attr('style', 'border-top-color: #6e0000 !important');
  jQuery('.ccc-usermarketing-promo-unit .small-button').attr('style','background-color: #6e0000 !important');
}

if(jQuery('.ccc-banner__headline > .ccc-banner__link').length > 0) {
  if(jQuery('.ccc-banner__headline > .ccc-banner__link').text() == "Design & Learning Center") {
    jQuery('.big-article__image').attr('style', 'border-top-color: #6e0000 !important');
    jQuery('.article-labels').remove();
    jQuery('.ccc-usermarketing-promo.sponsored').attr('style', 'border-top-color: #6e0000 !important');
    jQuery('.ccc-usermarketing-promo-unit .small-button').attr('style','background-color: #6e0000 !important');
  }
}

});
