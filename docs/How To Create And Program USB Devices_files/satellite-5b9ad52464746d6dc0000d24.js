_satellite.pushBlockingScript(function(event, target, $variables){
  var txtToinsert = '<div style="padding-top: 15px;font-style: italic;">Publish our articles on your site. <a href="https:\/\/electronics.informa.com\/contentstream\/" target="_blank" style="">Learn how...</a></div>';

if(jQuery("article.article-type-article").length > 0) {
  var divToinsertBefore = (jQuery('.secondary-tags').length == 1) ? '.secondary-tags' : '.comments-wrapper';
  jQuery(divToinsertBefore).before(txtToinsert);
  
}

Drupal.behaviors.addTextNativeInfinite = {
  attach: function (context) {
    if (jQuery(context).hasClass("jscroll-added") && jQuery(context).find("article.article-type-article").length == 1) {
			var divToinsertBefore = (jQuery('.secondary-tags', context).length == 1) ? '.secondary-tags' : '.comments-wrapper';
  		jQuery(divToinsertBefore, context).before(txtToinsert);
    }
  }
};
});
