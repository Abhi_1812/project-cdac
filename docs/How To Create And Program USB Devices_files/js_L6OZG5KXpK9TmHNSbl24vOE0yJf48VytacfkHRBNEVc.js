(function ($) {
  "use strict";

  var STICKY_LIST = {},
    IID = 0,
    DELAY_DISABLE_STICKY = 0,
    $win = $(window),
    FIX_FOR_IE = true;

  function Sticky (id, $el, options) {
    this.defaultSettings = {
      scrollTop: 50,
      parentElement: {
        min: 65,
        max: 126
      }
    };
    this.id = id;
    this.stickyClass = 'sticky';
    this.status = false;
    this.setElement($el);
    this.init($.extend(this.defaultSettings, options));
  }

  Sticky.prototype.changeSetting = function (settings) {
    this.settings = $.extend(this.defaultSettings, settings);
  };

  Sticky.prototype.init = function (settings) {
    this.settings = settings;
    this.$parent = $(settings.parent);

    this.position = {
      self: 0,
      parent: 0
    };

    this.position = $.extend(this.position, this.countPosition());
    this.position.self = this.$el.offset().top;

    if ($(window).scrollTop() > this.settings.scrollTop) {
      this.scrolling();
    }
  };

  Sticky.prototype.destroy = function () {
    this.$el.trigger('custom_sticky:destroy', [this]);
    delete STICKY_LIST[this.id];
  };

  Sticky.prototype.getParentHeight = function () {
    var style = this.$parent.attr('style');
    var height = typeof style !== 'undefined' ? style.match(/height: (\w+)px;/im) : null;

    if (height !== null && height.length > 1) {
      return parseInt(height[1], 10);
    }

    return $win.scrollTop() > this.settings.scrollTop ?
      this.settings.parentElement.max : this.settings.parentElement.min;
  };

  Sticky.prototype.resize = function () {
    this.$el.trigger('custom_sticky:resize', [this]);
  };

  Sticky.prototype.scrolling = function () {
    this.position = $.extend(this.position, this.countPosition());

    if (this.position.parent >= this.position.self) {

      this.$el.css({
        top: this.getParentHeight()
      });

      if (this.status) {
        return;
      }

      this.status = true;
      this.$el.addClass(this.stickyClass);
      this.$el.trigger('custom_sticky:sticky', [this]);
    } else {
      if (this.status === false) {
        return;
      }

      this.status = false;
      this.$el.removeClass(this.stickyClass);
      this.$el.trigger('custom_sticky:unsticky', [this]);
    }
  };

  Sticky.prototype.countPosition = function () {
    return {
      self: !this.status ? this.$el.offset().top : this.position.self,
      parent: this.$parent.offset().top + this.getParentHeight()
    };
  };

  Sticky.prototype.setElement = function ($el) {
    this.$el = $el;
    this.$el.wrap('<div class="sticky-wrapper">');
    this.$el.parent().css({
      width: '100%'
    });
    this.$place = this.$el.parent();
  };

  $.fn.CustomSticky = function (options) {
    $(this).each(function () {
      STICKY_LIST[IID] = new Sticky(IID, $(this), options);
      IID++;
    });

    return this;
  };

  function actionScroll() {
    if (STICKY_LIST.length === 0) {
      return;
    }

    for (var key in STICKY_LIST) {
      if (STICKY_LIST.hasOwnProperty(key)) {
        STICKY_LIST[key].scrolling();
      }
    }
  }

  function actionResize() {
    if (STICKY_LIST.length === 0) {
      return;
    }

    for (var key in STICKY_LIST) {
      if (STICKY_LIST.hasOwnProperty(key)) {
        STICKY_LIST[key].resize();
      }
    }
  }

  function toStatic(sticky) {
    sticky.$el.css({
      width: '',
      boxShadow: '',
      maxWidth: '1200px',
      position: 'relative',
      top: 0,
      left: 0
    });
    sticky.$el.removeClass('sticky');
    sticky.destroy();
  }

  function toSticky(sticky) {
    toResize(sticky);

    for (var k in STICKY_LIST) {
      if (STICKY_LIST.hasOwnProperty(k) && STICKY_LIST[k].status && STICKY_LIST[k].id !== sticky.id) {
        toStatic(STICKY_LIST[k]);
      }
    }

    if (DELAY_DISABLE_STICKY) {
      setTimeout(function () {
        toStatic(sticky);
      }, DELAY_DISABLE_STICKY);
    }
  }

  function toResize(sticky) {
    if (!sticky.status) {
      return;
    }

    var leftBar = $('.js-sticky-leftcol');
    var container = $('.l-sidebar');
    var left = leftBar.length ? leftBar.offset().left : $('main .l-content').offset().left;
    var width = sticky.$el.innerWidth();
    var wrapperWidth = sticky.$place.innerWidth();

    if (leftBar.length && !sticky.$el.hasClass('banner-top-wrapper')) {
      left = leftBar.offset().left + leftBar.innerWidth();
    } else if (container.hasClass('collapsible')) {
      left = 0;
    }

    if (container.hasClass('collapsible')) {
      width = '100%';
    } else if ($win.width() > width && width > wrapperWidth) {
      width = wrapperWidth;
    } else if ($win.width() < width) {
      width = $win.width();
    }

    sticky.$el.css({
      width: width,
      left: left,
      position: 'fixed'
    });
  }

  function initCustomStickyForElement(selector, context, options) {
    var setting = $.extend({
      parent: '.js-header',
      scrollTop: 50,
      parentElement: {
        min: 65,
        max: 126
      }
    }, options);
    var is_not_sticky_sidebar_and_interstitial = !$('.l-sidebar .js-sticky-leftcol').length && selector === '.interstitial-ad-wrapper';
    var $mainarea = $('.l-main-area');

    if ($('.js-header').length) {
      var $stickyElement = $(selector, context);
        return $stickyElement.CustomSticky(setting)
            .on('custom_sticky:sticky', function (e, sticky) {
                toSticky(sticky);

                if (is_not_sticky_sidebar_and_interstitial) {
                    var mainareaLeft = Number($mainarea.css('padding-left').replace('px', '')) + $mainarea.offset().left;
                    $stickyElement.css('left', mainareaLeft);
                }
            })
            .on('custom_sticky:unsticky', function (e, sticky) {
                toStatic(sticky);

                if (is_not_sticky_sidebar_and_interstitial) {
                    $stickyElement.css('left', 0);
                }
            })
            .on('custom_sticky:resize', function (e, sticky) {
                toResize(sticky);
            });
    }
  }

  function isEnableInAllArticlesPages() {
    return typeof Drupal.settings.penton_custom_dfp !== 'undefined' &&
      Drupal.settings.penton_custom_dfp.current_type === 'article';
  }

  function isEnable() {
    return typeof Drupal.settings.penton_custom_dfp !== 'undefined' &&
      !!Drupal.settings.penton_custom_dfp.enable_sticky;
  }

  Drupal.behaviors.infiniteSticky = {
    attach: function (context) {
      if (!isEnable() || !isEnableInAllArticlesPages()) {
        return;
      }

      initCustomStickyForElement('.interstitial-ad-wrapper', context);
    }
  };

  $(function () {
    if (!isEnable() || !isEnableInAllArticlesPages()) {
      return;
    }

    $(document).on('scroll', function () {
      if (FIX_FOR_IE) {
        FIX_FOR_IE = false;
        setTimeout(actionScroll, 100);
      } else {
        actionScroll();
      }
    });

    $win.resize(actionResize);

    if (Drupal.settings.penton_custom_dfp.lifetime_banner) {
      DELAY_DISABLE_STICKY = Drupal.settings.penton_custom_dfp.lifetime_banner;
    }

    if (Drupal.settings.penton_custom_dfp.do_byline) {
      DELAY_DISABLE_STICKY = 0;
    }

    initCustomStickyForElement('.banner-top-wrapper', $('body'));

    // it's need for fix position after hide a legal communication alert
    $('.js-penton-legal-comm-ajax-output-alert')
      .on('click', '.js-legal-comm-message-confirm', actionScroll);
  });
} (jQuery));
;
var eloquaTrackingEnabled = false;
var eloquaSiteId = '';

// Returns input with customer GUID to paste into form.
function getEloquaCustomerGUIDinput(callback) {
  callback = callback || $.noop;
  if (!eloquaTrackingEnabled || typeof eloquaSiteId === 'undefined' || !eloquaSiteId) {
    return;
  }
  _getCustomerGUID(function(GUID) {
    var customerGUIDinput = document.createElement('input');
    customerGUIDinput.type = 'hidden';
    customerGUIDinput.name = 'elqCustomerGUID';
    customerGUIDinput.value = GUID;
    return callback(customerGUIDinput);
  });
}

// Wrapper function for GetElqCustomerGUID.
function _getCustomerGUID(callback) {
  callback = callback || $.noop;
  _requestGUIDfunction(function() {
    return callback(GetElqCustomerGUID());
  });
}

// Makes a request for GetElqCustomerGUID function to Eloqua.
// Replaces _elqQ.push(['elqGetCustomerGUID']) because we need
// to implement onload callback.
function _requestGUIDfunction(callback) {
  callback = callback || $.noop;
  if (typeof GetElqCustomerGUID === 'function') {
    return callback();
  }
  var host = '//s' + eloquaSiteId + '.t.eloqua.com/';
  var url = 'visitor/v200/svrGP';
  var dat = new Date;
  var time = dat.getMilliseconds();
  var get = '?pps=70&siteid=' + eloquaSiteId + '&ref=' + encodeURI(document.referrer) + '&ms=' + time;
  var f = document.createElement('script');
  f.type = 'text/javascript';
  f.src = host + url + get;
  f.async = true;
  f.onload = function() {
    if (typeof GetElqCustomerGUID !== 'function') {
      console.log('Could not retreive GetElqCustomerGUID function');
      return;
    }
    return callback();
  }
  document.getElementsByTagName('head')[0].appendChild(f)
}

(function($) {
  Drupal.behaviors.penton_eloqua_api = {
    attach: function (context, settings) {
      var elqSettings = settings.penton_eloqua_api;
      eloquaTrackingEnabled = elqSettings.tracking_enabled;
      if (typeof elqSettings === 'undefined' || !eloquaTrackingEnabled) {
        return;
      }
      eloquaSiteId = elqSettings.eloqua_site_id;
      if (typeof eloquaSiteId === 'undefined' || !eloquaSiteId) {
        return;
      }

      window._elqQ = window._elqQ || [];
      window._elqQ.push(['elqSetSiteId', eloquaSiteId]);
      window._elqQ.push(['elqTrackPageView']);

      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
      s.src = '//img.en25.com/i/elqCfg.min.js';
      var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    }
  }
})(jQuery);
;
