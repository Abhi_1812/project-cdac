#ifndef __EINT_H
#define __EINT_H

#include "LPC17xx.h"

#define EINT2	2

void eint2_init(void);
void enable_USB_interrupts(void);
void EINT2_IRQHandler(void);

#endif

