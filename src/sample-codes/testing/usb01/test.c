#include<stdio.h>
#include<fcntl.h>


int main(){

	int ret,fd;

	char buff[10];

	printf("It is a test application\n");

	fd = open("/dev/pen0",O_RDONLY,0666);
	if(fd<0)
		printf("Device not open\n");

	ret = read(fd,buff,sizeof(buff));
	if(ret<=0)
		printf("read is not working\n");
	else
		printf(">> %d\n",ret);

	printf("%s\n",buff);

	close(fd);

}
