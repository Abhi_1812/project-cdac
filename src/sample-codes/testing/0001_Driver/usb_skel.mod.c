#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x54efefb9, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd40f4156, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0xebc2c3ef, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf767b25e, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x6c3fb850, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe1900c49, __VMLINUX_SYMBOL_STR(usb_register_dev) },
	{ 0xc671e369, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x76c5c742, __VMLINUX_SYMBOL_STR(usb_bulk_msg) },
	{ 0xb5419b40, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x74efe755, __VMLINUX_SYMBOL_STR(usb_deregister_dev) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("usb:vFFFFp0004d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "8E2B7E9FFECC23FE9598DAA");
