#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/uaccess.h>
#include<linux/slab.h>
#include<linux/kfifo.h>


int a;

#define MIN(a,b) (((a) <= (b)) ? (a) : (b))


#define MAX_PKT_SIZE 64

static struct usb_device *device;
static struct usb_class_driver class;
static unsigned char bulk_buf[MAX_PKT_SIZE];

typedef struct my_device {
    struct usb_device   *udev;      /* usb device for this device */
    struct usb_interface    *interface; /* interface for this device */
    unsigned char       minor;      /* minor value */
    unsigned char *     bulk_in_buffer; /* the buffer to in data */
    size_t          bulk_in_size;   /* the size of the in buffer */
    __u8            bulk_in_add;    /* bulk in endpoint address */
    __u8            bulk_out_add;   /* bulk out endpoint address */
    struct kref     kref;       /* module references counter */
}mydev_t;

mydev_t *dev;

static void set_bulk_address ( struct usb_interface *interface)
{
    struct usb_endpoint_descriptor *endpoint;
    struct usb_host_interface *iface_desc;
    int i=0;

    iface_desc = interface->cur_altsetting;
    for (i = 0; i < iface_desc->desc.bNumEndpoints-1; ++i) {
      endpoint = &iface_desc->endpoint[i].desc;


	printk(KERN_INFO " %s>> %d i/f is now probed\n",THIS_MODULE->name,iface_desc->desc.bInterfaceNumber );


        /* check for bulk endpoint */
        if ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) 
            == USB_ENDPOINT_XFER_BULK){

            /* bulk in */
            if(endpoint->bEndpointAddress & USB_DIR_IN) {
                dev->udev = device;
		dev->bulk_in_add = endpoint->bEndpointAddress;
                dev->bulk_in_size = endpoint->wMaxPacketSize;
                dev->bulk_in_buffer = kmalloc(dev->bulk_in_size,
                                GFP_KERNEL);
                if (!dev->bulk_in_buffer)
                    printk("Could not allocate bulk buffer");
            }

            /* bulk out */
            else
                dev->bulk_out_add = endpoint->bEndpointAddress; 
        
	
        	printk(">>%s>>%x\n",THIS_MODULE->name,endpoint->bEndpointAddress);
		
	}
   }
	
}


static int pen_open(struct inode *i, struct file *f)
{
	printk(KERN_INFO "pen_open() called.\n");
	
	return 0;
}
static int pen_close(struct inode *i, struct file *f)
{
	printk(KERN_INFO "pen_close() called.\n");
	return 0;
}
static ssize_t pen_read(struct file *f, char __user *buf, size_t cnt, loff_t *off)
{
	int retval;
	int read_cnt;

	//struct usb_endpoint_descriptor *endpoint;
	printk(KERN_INFO "pen_read() called.\n");

        printk(">>%s>>%x\n",THIS_MODULE->name,dev->bulk_in_add);
	/* Read the data from the bulk endpoint */
	retval = usb_bulk_msg(dev->udev, usb_rcvbulkpipe(device, dev->bulk_in_add),
		 	      &a, MAX_PKT_SIZE, &read_cnt, 5000);
	printk(KERN_INFO "%d read\n",retval);
	if (retval)
	{
		printk(KERN_ERR "Bulk message returned %d\n", retval);
		return retval;
	}

	if (copy_to_user(buf, bulk_buf, MIN(cnt, read_cnt)))
	{
		return -EFAULT;
	}
	return MIN(cnt, read_cnt);
}

static ssize_t pen_write(struct file *f, const char __user *buf, size_t cnt,
									loff_t *off)
{
	int retval;
	int wrote_cnt = MIN(cnt, MAX_PKT_SIZE);
//	struct usb_endpoint_descriptor *endpoint;
	printk(KERN_INFO "pen_write() called.\n");

	if (copy_from_user(bulk_buf, buf, MIN(cnt, MAX_PKT_SIZE)))
	{
		return -EFAULT;
	}

	/* Write the data into the bulk endpoint */
	retval = usb_bulk_msg(device, usb_sndbulkpipe(device, dev->bulk_out_add),
			bulk_buf, MIN(cnt, MAX_PKT_SIZE), &wrote_cnt, 5000);
	if (retval)
	{
		printk(KERN_ERR "Bulk message returned %d\n", retval);
		return retval;
	}

	return wrote_cnt;
}

static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = pen_open,
	.release = pen_close,
	.read = pen_read,
	.write = pen_write,
};

static int pen_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	int retval;
	printk(KERN_INFO "pen_probe() called.\n");

	device = interface_to_usbdev(interface);

	class.name = "usb/pen%d";
	class.fops = &fops;
	if ((retval = usb_register_dev(interface, &class)) < 0)
	{
		/* Something prevented us from registering this driver */
		printk(KERN_ERR "Not able to get a minor for this device.");
	}
	else
	{
		printk(KERN_INFO "Minor obtained: %d\n", interface->minor);
	}

        dev = (mydev_t*)kmalloc(sizeof(mydev_t),GFP_KERNEL);

	set_bulk_address(interface);
	return retval;
}

static void pen_disconnect(struct usb_interface *interface)
{
	
	usb_deregister_dev(interface, &class);

	kfree(dev->bulk_in_buffer);
	kfree(dev);

	printk(KERN_INFO "pen_disconnect() called.\n");
}

/* Table of devices that work with this driver */
static struct usb_device_id pen_table[] =
{
	{ USB_DEVICE(0xffff, 0x0004) },
	{} /* Terminating entry */
};
MODULE_DEVICE_TABLE (usb, pen_table);

static struct usb_driver pen_driver =
{
	.name = "pen_driver",
	.probe = pen_probe,
	.disconnect = pen_disconnect,
	.id_table = pen_table,
};

static int __init pen_init(void)
{
	int result;
	printk(KERN_INFO "pen_init() called.\n");
	
	/* Register this driver with the USB subsystem */
	if ((result = usb_register(&pen_driver)))
	{
		printk(KERN_ERR "usb_register failed. Error number %d", result);
		return -1;
	}
	return result;
}

static void __exit pen_exit(void)
{
	printk(KERN_INFO "pen_exit() called.\n");
	/* Deregister this driver with the USB subsystem */
	usb_deregister(&pen_driver);
}

module_init(pen_init);
module_exit(pen_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anil Kumar Pugalia <email@sarika-pugs.com>");
MODULE_DESCRIPTION("USB Pen Device Driver");
