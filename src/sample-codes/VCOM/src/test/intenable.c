#include "LPC17xx.h"
#include "eint.h"

void enable_USB_interrupts(void)
{
	NVIC_EnableIRQ(USB_IRQn);	
}
